# alarmCollectByFlink

#### 介绍
告警采集平台，Flink读取Kafka消息，实时检测三个场景，一是当前访问IP或者域名是否黑名单地址，二是当前连接端口是否敏感生产环境端口，三是检测一段时间内连续登录失败后突然又登录成功的场景。

#### 软件架构
- Flink1.14.4
- Kafka-2.8
- ElasticSearch-7.3.2
- java-8


#### 打包命令

mvn clean package assembly:single -Dmaven.test.skip=true


