package com.data.dev.key;

import com.data.dev.common.javabean.BaseBean;

/**
 * @author wangxiaoming-ghq
 * 2022年6月17日15:26:19
 * kafka集群的配置类
 */
public class KafkaKey extends BaseBean {

    /**
     * offset提交策略
     */
    public static final  String AUTO_OFFSET_RESET = "auto.offset.reset";

    /**
     * kafka认证协议
     */
    public static final  String SECURITY_PROTOCOL = "security.protocol";

    /**
     * 认证协议类型
     */
    public static final  String SASL_MECHANISM = "sasl.mechanism";

    /**
     * 认证配置
     */
    public static final  String SASL_JAAS_CONFIG = "sasl.jaas.config";

    /**
     * key序列化器
     */
    public static final  String KEY_DESERIALIZER = "key.deserializer";

    /**
     * value序列化器
     */
    public static final  String VALUE_DESERIALIZER = "value.deserializer";

}
