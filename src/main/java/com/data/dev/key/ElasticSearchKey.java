package com.data.dev.key;

import com.data.dev.common.javabean.BaseBean;

/**
 * 2022年6月17日15:24:57
 * @author wangxiaoming-ghq
 * 获取ES全局连接信息
 */
public class ElasticSearchKey extends BaseBean {

    /**
     * 连接主机，多个已英文逗号隔开
     */
    public static final String  HOST = "host";

    /**
     * 连接端口
     */
    public static final String  PORT = "port";

    /**
     * 连接密码
     */
    public static final String  PASSWORD = "password";

    /**
     * 连接用户名
     */
    public static final String  USERNAME = "username";

}
