package com.data.dev.key;

import com.data.dev.common.javabean.BaseBean;

/**
 * {
 *   "user": "yangxuefei-lhb",
 *   "client_ip": "10.68.6.182",
 *   "source": "login",
 *   "loginname": "yangxuefei-lhb@sinosig.com",
 *   "IP": "10.8.148.58",
 *   "timestamp": "17:58:12",
 *   "@timestamp": "2022-04-20T09:58:13.647Z",
 *   "ip": "10.7.231.25",
 *   "clienttype": "POP3",
 *   "result": "success",
 *   "@version": "1"
 * }
 *
 * user登录用户
 * client_ip 来源ip
 * source 类型
 * loginname 登录用户邮箱地址
 * ip 目标前端ip
 * timestamp 发送时间
 * &#064;timestamp  发送日期时间
 * IP 邮件日志发送来源IP
 * clienttype 客户端登录类型
 * result 登录状态
 */
public class MailMsgKey  extends BaseBean {

    public static final String MAIL_USER = "user";
    public static final String MAIL_CLIENT_IP = "client_ip";
    public static final String MAIL_SOURCE = "source";
    public static final String MAIL_LOGIN_NAME = "loginname";
    public static final String MAIL_SENDER_IP = "IP";
    public static final String MAIL_TIMESTAMP_TIME = "timestamp";
    public static final String MAIL_TIMESTAMP_DATETIME = "@timestamp";
    public static final String MAIL_TARGET_IP = "ip";
    public static final String MAIL_CLIENT_TYPE = "clienttype";
    public static final String  MAIL_RESULT = "result";
    public static final String  MAIL_VERSION = "@version";



}
