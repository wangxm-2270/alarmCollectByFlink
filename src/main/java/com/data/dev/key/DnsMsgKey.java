package com.data.dev.key;


import com.data.dev.common.javabean.BaseBean;

/**
 * {
 * 	"@timestamp":"2022-05-17T02:49:34.801Z",
 * 	"@metadata":{
 * 		"beat":"packetbeat",
 * 		"type":"_doc",
 * 		"version":"7.10.2"
 *        },
 * 	"ip":"10.0.0.10",
 * 	"dns":{
 * 		"question":{
 * 			"name":"sd.dnslog.cn",
 * 			"type":"AAAA"
 *        }
 *    },
 * 	"client_ip":"10.63.201.22",
 * 	"from":"165-15-ens3f1",
 * 	"type":"dns"
 * }
 *
 *
 * &#064;timestamp  日志时间
 * client_ip 请求来源ip
 * from 日志发送主机名称
 * type 日志类型
 * dns.question.name 请求解析的域名
 * dns.question.type 域名请求类型
 * ip dns服务器地址
 *
 */
public class DnsMsgKey extends BaseBean {
    public static final String DNS_QUESTION_NAME = "dns.question.name";
    public static final String DNS_QUESTION_TYPE = "dns.question.type";
    public static final String DNS_METADATA_BEAT = "metadata.beat";
    public static final String DNS_METADATA_TYPE = "metadata.type";
    public static final String DNS_METADATA_VERSION = "metadata.version";
    public static final String DNS_METADATA = "@metadata";
    public static final String DNS_TIMESTAMP = "timestamp";
    public static final String DNS_AT_TIMESTAMP = "@timestamp";

    public static final String DNS_TYPE = "type";
    public static final String DNS_IP = "ip";
    public static final String DNS_CLIENT_IP = "client_ip";
    public static final String DNS_FROM = "from";
    public static final String DNS_DNS = "dns";
}
