package com.data.dev.key;

import com.data.dev.common.javabean.BaseBean;

/**
 *    {
 *         "flow_start_time": 1650451868000,
 *         "app": 1,
 *         "server_total_packet": 3,
 *         "client_total_byte": 538,
 *         "client_total_packet": 5,
 *         "netlink": 1,
 *         "client_ip_addr": "10.128.80.29",
 *         "tcp_status": 7,
 *         "flow_end_time": 1650451869000,
 *         "client_port": 62765,
 *         "log_type": "TCP",
 *         "protocol": 615,
 *         "server_port": 80,
 *         "server_ip_addr": "10.7.44.80",
 *         "server_total_byte": 329,
 *         "host_name": "tz_tsa_90",
 *         "direction": 1
 *     }
 */
public class KelaiMsgKey extends BaseBean {
    public static String FLOW_START_TIME = "flow_start_time";
    public static String APP = "app";
    public static String SERVER_TOTAL_PACKET = "server_total_packet";
    public static String CLIENT_TOTAL_BYTE = "client_total_byte";
    public static String CLIENT_TOTAL_PACKET = "client_total_packet";
    public static String NETLINK = "netlink";
    public static String CLIENT_IP_ADDR = "client_ip_addr";
    public static String TCP_STATUS = "tcp_status";
    public static String FLOW_END_TIME = "flow_end_time";
    public static String CLIENT_PORT = "client_port";
    public static String LOG_TYPE = "log_type";
    public static String PROTOCOL = "protocol";
    public static String SERVER_PORT = "server_port";
    public static String SERVER_IP_ADDR = "server_ip_addr";
    public static String SERVER_TOTAL_BYTE = "server_total_byte";
    public static String HOST_NAME = "host_name";
    public static String DIRECTION = "direction";

    public static String WHITELIST_IP_PATH = "kafka.kelei.whitelist.ip.path";

}
