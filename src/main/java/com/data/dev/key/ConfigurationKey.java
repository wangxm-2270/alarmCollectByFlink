package com.data.dev.key;

import com.data.dev.common.exception.ConfErrorCode;
import com.data.dev.common.exception.AlarmException;
import com.data.dev.common.javabean.BaseBean;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

/**
 * @author wangxiaoming-ghq
 * 2022年6月17日15:23:58
 * 公共配置类
 */
public class ConfigurationKey extends BaseBean {

    public static  final  String APPLICATION_PROPERTIES_FILE_PATH =  "/app/data/cluster/conf/application.properties";
    public static  final  String KAFKA_DNS_CONSUMER_GROUP_ID =  "DNS_MSG_CONSUMER_TO_CHECK_BLACKLIST";
    public static  final  String KAFKA_KELAI_CONSUMER_GROUP_ID =  "DNS_MSG_CONSUMER_TO_SCAN_PORT";
    public static  final  String KAFKA_MAIL_CONSUMER_GROUP_ID =  "DNS_MSG_CONSUMER_TO_CHECK_LOGIN_FAIL";

    public static  final  int KAFKA_DNS_BLACKLIST_IP_CHECK_STATUS_CODE =  200;
    public static  final  String KAFKA_DNS_QUESTION_BLACKLIST_FILEPATH =  "kafka.question.blacklist.filepath";
    public static  final  String KAFKA_KELAI_WHITELIST_FILEPATH =  "kafka.kelei.whitelist.ip.path";

    public static  final  String KAFKA_DNS_SECURITY_PROTOCOL =  "kafka.dns.security.protocol";
    public static  final  String KAFKA_DNS_SASL_MECHANISM =  "kafka.dns.sasl.mechanism";
    public static  final  String KAFKA_DNS_SASL_JAAS_CONFIG =  "kafka.dns.sasl.jaas.config";
    public static  final  String KAFKA_DNS_BOOTSTRAP_SERVER =  "kafka.dns.bootstrap.servers";
    public static  final  String KAFKA_DNS_AUTO_OFFSET_RESET =  "kafka.dns.auto.offset.reset";
    public static  final  String KAFKA_DNS_KEY_DESERIALIZER =  "kafka.dns.key.deserializer";
    public static  final  String KAFKA_DNS_VALUE_DESERIALIZER =  "kafka.dns.value.deserializer";

    public static  final  String KAFKA_DNS_TOPIC_NAME =  "kafka.dns.topic.name";
    public static  final  String KAFKA_MAIL_TOPIC_NAME =  "kafka.mail.topic.name";
    public static  final  String KAFKA_KELAI_TOPIC_NAME =  "kafka.kelai.topic.name";

    public static final String  ES_HOST = "elasticsearch.host.ip";
    public static final String  ES_PASSWORD = "elasticsearch.password";
    public static final String  ES_USERNAME = "elasticsearch.username";
    public static final String  ES_PORT = "elasticsearch.host.port";


    public static final String IP_CIDR_252 = "10.114.0.0/16";
    public static final String IP_CIDR_249 = "10.113.0.0/16";
    public static final String ES_INDEX_NAME_PORT_SCAN = "elasticsearch.sink.index.name.portscan";
    public static final String ES_INDEX_NAME_LOGIN_FAIL = "elasticsearch.sink.index.name.loginFail";
    public static final String ES_INDEX_NAME_BLACKLIST = "elasticsearch.sink.index.name.blacklistAlarm";
    public static final String ES_INDEX_TYPE = "_doc";



    /**
     * 2022年6月17日15:24:23
     * 获取全局配置信息
     * @return map:全局配置信息
     */
    public static HashMap<String,String> getApplicationProps() {
        Properties props = new Properties();
        HashMap<String, String > propsMap = new HashMap<>();
        FileInputStream in = null;
        try {
            in = new FileInputStream(APPLICATION_PROPERTIES_FILE_PATH);
        } catch (FileNotFoundException e) {
            throw AlarmException.asAlarmException(ConfErrorCode.CONFIG_ERROR,
                    e);
        }
        try {
            props.load(in);
            for (String key : props.stringPropertyNames()) {
                System.out.println(key + ":" + props.getProperty(key));
                propsMap.put(key,props.getProperty(key));
            }
        } catch (IOException e) {
            throw AlarmException.asAlarmException(ConfErrorCode.CONFIG_ERROR,
                    e);
        }
        try {
            in.close();
        } catch (IOException e) {
            throw AlarmException.asAlarmException(ConfErrorCode.CONFIG_ERROR,
                    e);
        }

        return  propsMap;
    }
}
