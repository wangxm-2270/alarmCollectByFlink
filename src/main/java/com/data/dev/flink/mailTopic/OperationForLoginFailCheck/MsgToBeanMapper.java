package com.data.dev.flink.mailTopic.OperationForLoginFailCheck;

import com.data.dev.common.javabean.BaseBean;
import com.data.dev.common.javabean.kafkaMailTopic.MailMsg;
import com.data.dev.utils.MsgToJavaBeanUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.functions.MapFunction;

/**
 *  kafka消息持久化
 *  @author wangxiaoming-ghq 2022-06-01
 */
@Slf4j
public   class MsgToBeanMapper extends BaseBean implements MapFunction<String, MailMsg> {
    @Override
    public MailMsg map(String msgJson) throws Exception {
        return MsgToJavaBeanUtils.getMailMsgBean(msgJson);
    }
}