package com.data.dev.flink.keleiTopic.operationForPortScan;

import com.alibaba.fastjson.JSON;
import com.data.dev.common.javabean.BaseBean;
import com.data.dev.common.javabean.kafkaKelaiTopic.KelaiMsg;
import com.data.dev.utils.HttpUtils;
import com.data.dev.utils.IPUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.functions.MapFunction;

/**
 * 2022年6月17日15:09:14
 * @author wangxiaoming-ghq
 *  消息格式转换、匹配的告警推送到ES
 */
@Slf4j
public   class kelaiMsgToStringMapper extends BaseBean implements MapFunction<KelaiMsg,String > {

    @Override
    public String map(KelaiMsg kelaiMsg) throws Exception {
        doAlarmPush(kelaiMsg); //告警推送
        log.info("标准化消息体：{}",JSON.toJSONString(kelaiMsg));
        return JSON.toJSONString(kelaiMsg);
    }

    /**
     * 2022年6月17日14:47:53
     * @param kelaiMsg :当前构建的需要告警的事件
     */
    public void doAlarmPush(KelaiMsg kelaiMsg){
        String hostNameCurrent = kelaiMsg.getHost_name();
        String clientIpAddr = kelaiMsg.getClient_ip_addr();
        boolean isWhiteListIp = IPUtils.isWhiteListIp(clientIpAddr);
        if(isWhiteListIp){//如果是白名单IP，不告警
            log.info("当前登录用户【" + hostNameCurrent + "@" + clientIpAddr + "】属于白名单IP");
        }else {
            //IP归属查询结果、企业微信推送告警
            String user = HttpUtils.getUserByClientIp(clientIpAddr);
            HttpUtils.pushAlarmMsgToWechatWork(user,kelaiMsg.toString());
            log.info("推送告警");
        }
    }
}