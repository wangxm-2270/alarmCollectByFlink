package com.data.dev.flink.keleiTopic.operationForPortScan;

import com.data.dev.common.javabean.BaseBean;
import com.data.dev.common.javabean.kafkaKelaiTopic.KeleiMsgList;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.functions.MapFunction;

import java.util.Collections;

/**
 * 2022年6月17日15:19:48
 * @author wangxiaoming-ghq
 * 将原始消息数组转换为集合
 */
@Slf4j
public   class keleiMsgJsonArrayToKeleiListMapper extends BaseBean implements MapFunction<String, KeleiMsgList> {
    KeleiMsgList kml ;
    @Override
    public KeleiMsgList map(String msg) throws Exception {
        kml = new KeleiMsgList(Collections.singletonList(msg.replace("[", "").replace("]", "")));
         return kml;
    }
}
