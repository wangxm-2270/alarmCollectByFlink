package com.data.dev.flink.keleiTopic.operationForPortScan;

import com.data.dev.common.javabean.BaseBean;
import com.data.dev.common.javabean.kafkaKelaiTopic.KelaiMsg;
import com.data.dev.kafka.CheckSourceIpAndServerTotalBytes;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.functions.FilterFunction;


/**
 * ②　消费kelai topic，筛选：源IP为10.252.0.0/16或者10.249.0.0/16,响应数据（server_total_byte）大小不超200。
 * ③　告警经处理逻辑: 其中包含白名单比对 ,白名单内的IP忽略，不进行告警
 */
@Slf4j
public class kelaiMsgFilter extends BaseBean implements FilterFunction<KelaiMsg> {

    @Override
    public boolean filter(KelaiMsg kelaiMsg) {

        String clientIpAddr = kelaiMsg.getClient_ip_addr();

        log.info("检查ip：{}",clientIpAddr);
        boolean isServerTotalByteLessOf200 = CheckSourceIpAndServerTotalBytes.isServerTotalByteLessOf200(kelaiMsg.getServer_total_byte());
        boolean isIpBelongOfCIDR =  CheckSourceIpAndServerTotalBytes.isIpBelongOfCIDR(clientIpAddr);
        log.info("isIpBelongOfCIDR:{}",isIpBelongOfCIDR);
        boolean isWhiteListIp = CheckSourceIpAndServerTotalBytes.isWhiteListIp(clientIpAddr);

        return isIpBelongOfCIDR && isServerTotalByteLessOf200 && !isWhiteListIp;
    }
}