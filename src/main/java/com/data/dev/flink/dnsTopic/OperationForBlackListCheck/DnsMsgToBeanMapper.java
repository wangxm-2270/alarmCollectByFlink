package com.data.dev.flink.dnsTopic.OperationForBlackListCheck;

import com.data.dev.common.javabean.BaseBean;
import com.data.dev.common.javabean.kafkaDnsTopic.DnsMsg;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.functions.MapFunction;

import static com.data.dev.utils.MsgToJavaBeanUtils.getDnsMsgBean;

/**
 *  kafka消息持久化
 *  @author wangxiaoming-ghq 2022-06-01
 */
@Slf4j
public   class DnsMsgToBeanMapper extends BaseBean implements MapFunction<String, DnsMsg> {
    @Override
    public DnsMsg map(String msg) throws Exception {
        return   getDnsMsgBean(msg);
    }
}