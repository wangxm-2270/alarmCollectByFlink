package com.data.dev.flink.dnsTopic.OperationForBlackListCheck;

import com.alibaba.fastjson.JSON;
import com.data.dev.common.javabean.BaseBean;
import com.data.dev.common.javabean.kafkaDnsTopic.DnsMsg;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.functions.MapFunction;

/**
 *  持久化类与List的元素映射
 *  @author wangxiaoming-ghq 2022-06-01
 */
@Slf4j
public   class DnsMsgToStrMapper extends BaseBean implements MapFunction<DnsMsg, String> {
    @Override
    public String  map(DnsMsg dnsMsg) throws Exception {
        return JSON.toJSONString(dnsMsg);
    }
}