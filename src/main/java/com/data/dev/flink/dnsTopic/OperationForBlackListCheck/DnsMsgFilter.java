package com.data.dev.flink.dnsTopic.OperationForBlackListCheck;

import com.data.dev.common.javabean.BaseBean;
import com.data.dev.common.javabean.kafkaDnsTopic.DnsMsg;
import com.data.dev.kafka.CheckBlackListByDnsName;
import com.data.dev.utils.IPUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.functions.FilterFunction;


/**
 * ②　消费mail主题的消息，过滤其中login的事件
 * @author wangxiaoming-ghq 2022-06-01
 */
@Slf4j
public class DnsMsgFilter extends BaseBean implements FilterFunction<DnsMsg> {
    @Override
    public boolean filter(DnsMsg dnsMsg) throws Exception {
        String clientIp = dnsMsg.getClient_ip();
        String dnsName = dnsMsg.getDns().getQuestion().getName();
        boolean isBlacklistDns = CheckBlackListByDnsName.isBlackListDnsName(dnsName);
        boolean isProductEnvIP = IPUtils.isProductEnvIp(clientIp);

        log.info("【 " + dnsMsg.getDns().getQuestion().getName() + " 】 是否为黑名单的DNS -------->  【 " + isBlacklistDns + " 】");
        log.info("【 " + dnsMsg.getClient_ip() + " 】 是否为生产IP -------->  【 " + isProductEnvIP + " 】");

        //IP归属查询结果、企业微信推送告警

        if(isBlacklistDns && isProductEnvIP){

            //String user = HttpUtils.getUserByClientIp(clientIp);
            String user = "gaohaitao-ghq";


            log.info("DNS BLACKLIST 【 " + dnsMsg + " 】 PUSH TO ELASTICSERACH ");
//            boolean isPushWechatSuccess = HttpUtils.pushAlarmMsgToWechatWork(user,dnsMsg.toString());
//            log.info("本次告警推送企业微信是否成功： " + isPushWechatSuccess);
        }

        return isBlacklistDns && isProductEnvIP;
    }
}