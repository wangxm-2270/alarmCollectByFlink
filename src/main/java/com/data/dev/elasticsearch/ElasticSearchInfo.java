package com.data.dev.elasticsearch;


import com.data.dev.common.javabean.BaseBean;
import com.data.dev.key.ConfigurationKey;
import java.util.HashMap;

/**
 * 2022年6月17日15:13:16
 * @author wangxiaoming-ghq 2022-05-15
 * ES写入工具类
 */

public class ElasticSearchInfo extends BaseBean {
    public static final long serialVersionUID = 2L;

    /**
     * ES连接信息初始化
     */
    public static final HashMap<String,String> ES_PROPS_MAP = ConfigurationKey.getApplicationProps();

    /**
     * ES写入索引名称
     */
    public static final String ES_PORT_SCAN_INDEX_NAME = ES_PROPS_MAP.get(ConfigurationKey.ES_INDEX_NAME_PORT_SCAN);

    /**
     * ES写入索引类型
     */
    public static final String ES_INDEX_TYPE_DEFAULT = ConfigurationKey.ES_INDEX_TYPE;

    /**
     * 黑名单访问逻辑结果写入目标索引
     */
    public static final String ES_BLACKLIST_INDEX_NAME = ES_PROPS_MAP.get(ConfigurationKey.ES_INDEX_NAME_BLACKLIST);

    /**
     * 连续登录失败场景写入目标索引
     */
    public static final String ES_LOGIN_FAIL_INDEX_NAME = ES_PROPS_MAP.get(ConfigurationKey.ES_INDEX_NAME_LOGIN_FAIL);
}
