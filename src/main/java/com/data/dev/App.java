package com.data.dev;

import com.data.dev.flink.dnsTopic.main.DnsMsg;
import com.data.dev.flink.keleiTopic.main.KelaiMsg;
import com.data.dev.flink.mailTopic.main.MailMsg;
import lombok.extern.slf4j.Slf4j;


/**
 * 传参式的调用
 * 0 ：启动dns消息解析，推送dns黑名单告警
 * 1 ：启动mail消息解析，推送连续登录失败后成功的告警
 * 2 ：启动kelai消息解析，推送端口扫描场景的告警
 */
@Slf4j
public class App {

    public static void main(String[] args) {
        if(null == args || args.length==0){
             System.exit(0);
        }

        String runCode = args[0];
        switch (runCode){

            case "0" :
                log.info("执行DNS主题解析，推送访问dns黑名单的告警");
                DnsMsg.execute();
                break;
            case "1" :
                log.info("执行Mail主题解析,判断3分钟内连续至少20次次登陆失败后成功并进行告警");
                MailMsg.execute();
                break;
            case "2" :
                log.info("执行kelai主题解析,将访问10.252.0.0/16及10.249.0.0/16进行告警");
                KelaiMsg.execute();
                break;
            default :
                log.error("请传入执行参数：\n" +
                        "     * 0 ：启动dns消息解析，推送dns黑名单告警\n" +
                        "     * 1 ：启动mail消息解析，推送连续登录失败后成功的告警\n" +
                        "     * 2 ：启动kelai消息解析，推送端口扫描场景的告警\n") ;

        }

    }
}