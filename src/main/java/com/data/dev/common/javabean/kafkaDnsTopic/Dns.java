package com.data.dev.common.javabean.kafkaDnsTopic;

import com.data.dev.common.javabean.BaseBean;
import lombok.Data;

import java.util.Objects;

/**
 * Dns消息解析
 * 2022年6月17日15:11:27
 * @author wangxiaoming-ghq 2022-05-15
 */

@Data
public class Dns  extends BaseBean  {
    Question question;

    public Dns(Question question) {
        this.question = question;
    }

    public Dns() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Dns)) return false;
        Dns dns = (Dns) o;
        return getQuestion().equals(dns.getQuestion());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getQuestion());
    }

    @Override
    public String toString() {
        return "{" +
                "\"question\":" + question +
                "}";
    }
}
