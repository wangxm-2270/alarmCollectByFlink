package com.data.dev.common.javabean.kafkaDnsTopic;

import com.data.dev.common.javabean.BaseBean;
import lombok.Data;

import java.util.Objects;
import java.util.StringJoiner;


/**
 * Dns消息解析
 * 2022年6月17日15:11:27
 * @author wangxiaoming-ghq
 *
 */
@Data
public class DnsMsg extends BaseBean {
    public String timestamp;
    public MetaData  metaData;
    public String ip;
    public Dns dns;
    public String client_ip;
    public String from;
    public String type;

    public DnsMsg() {
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", DnsMsg.class.getSimpleName() + "[", "]")
                .add("timestamp=" + timestamp + "")
                .add("metaData=" + metaData)
                .add("ip=\"" + ip + "\"")
                .add("dns=" + dns)
                .add("client_ip=\"" + client_ip + "\"")
                .add("from=\"" + from + "\"")
                .add("type=\"" + type + "\"")
                .toString();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DnsMsg)) return false;
        DnsMsg dnsMsg = (DnsMsg) o;
        return getTimestamp().equals(dnsMsg.getTimestamp()) && getMetaData().equals(dnsMsg.getMetaData()) && getIp().equals(dnsMsg.getIp()) && getDns().equals(dnsMsg.getDns()) && getClient_ip().equals(dnsMsg.getClient_ip()) && getFrom().equals(dnsMsg.getFrom()) && getType().equals(dnsMsg.getType());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTimestamp(), getMetaData(), getIp(), getDns(), getClient_ip(), getFrom(), getType());
    }


}
