package com.data.dev.common.javabean.kafkaDnsTopic;

import com.data.dev.common.javabean.BaseBean;
import lombok.Data;

import java.util.Objects;

/**
 * 	"dns":{
 * 		"question":{
 * 			"name":"sd.dnslog.cn",
 * 			"type":"AAAA"
 *                }
 *           }
 */
@Data
public class Question extends BaseBean {
    public String name;
    public String type;


    public Question() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Question)) return false;
        Question question = (Question) o;
        return getName().equals(question.getName()) && getType().equals(question.getType());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getType());
    }

    @Override
    public String toString() {
        return "{" +
                "\"name\":\"" + name + "\"" +
                ", \"type\":\"" + type + "\"" +
                "}";
    }
}
