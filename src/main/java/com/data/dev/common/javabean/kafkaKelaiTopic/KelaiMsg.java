package com.data.dev.common.javabean.kafkaKelaiTopic;

import lombok.Data;

import java.util.Objects;

/**
 *    {
 *         "flow_start_time": 1650451868000,
 *         "app": 1,
 *         "server_total_packet": 3,
 *         "client_total_byte": 538,
 *         "client_total_packet": 5,
 *         "netlink": 1,
 *         "client_ip_addr": "10.128.80.29",
 *         "tcp_status": 7,
 *         "flow_end_time": 1650451869000,
 *         "client_port": 62765,
 *         "log_type": "TCP",
 *         "protocol": 615,
 *         "server_port": 80,
 *         "server_ip_addr": "10.7.44.80",
 *         "server_total_byte": 329,
 *         "host_name": "tz_tsa_90",
 *         "direction": 1
 *     }
 */
@Data
public class KelaiMsg {
    public long flow_start_time;
    public int app;
    public int server_total_packet;
    public int client_total_byte;
    public int client_total_packet;
    public int netlink;
    public String client_ip_addr;
    public int tcp_status;
    public long flow_end_time;
    public int client_port;
    public String log_type;
    public int protocol;
    public int server_port;
    public String server_ip_addr;
    public int server_total_byte;
    public String host_name;
    public int direction;

    public KelaiMsg(long flow_start_time, int app, int server_total_packet, int client_total_byte, int client_total_packet, int netlink, String client_ip_addr, int tcp_status, long flow_end_time, int client_port, String log_type, int protocol, int server_port, String server_ip_addr, int server_total_byte, String host_name, int direction) {
        this.flow_start_time = flow_start_time;
        this.app = app;
        this.server_total_packet = server_total_packet;
        this.client_total_byte = client_total_byte;
        this.client_total_packet = client_total_packet;
        this.netlink = netlink;
        this.client_ip_addr = client_ip_addr;
        this.tcp_status = tcp_status;
        this.flow_end_time = flow_end_time;
        this.client_port = client_port;
        this.log_type = log_type;
        this.protocol = protocol;
        this.server_port = server_port;
        this.server_ip_addr = server_ip_addr;
        this.server_total_byte = server_total_byte;
        this.host_name = host_name;
        this.direction = direction;
    }

    public KelaiMsg() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof KelaiMsg)) return false;
        KelaiMsg kelaiMsg = (KelaiMsg) o;
        return getFlow_start_time() == kelaiMsg.getFlow_start_time() && getApp() == kelaiMsg.getApp() && getServer_total_packet() == kelaiMsg.getServer_total_packet() && getClient_total_byte() == kelaiMsg.getClient_total_byte() && getClient_total_packet() == kelaiMsg.getClient_total_packet() && getNetlink() == kelaiMsg.getNetlink() && getTcp_status() == kelaiMsg.getTcp_status() && getFlow_end_time() == kelaiMsg.getFlow_end_time() && getClient_port() == kelaiMsg.getClient_port() && getProtocol() == kelaiMsg.getProtocol() && getServer_port() == kelaiMsg.getServer_port() && getServer_total_byte() == kelaiMsg.getServer_total_byte() && getDirection() == kelaiMsg.getDirection() && getClient_ip_addr().equals(kelaiMsg.getClient_ip_addr()) && getLog_type().equals(kelaiMsg.getLog_type()) && getServer_ip_addr().equals(kelaiMsg.getServer_ip_addr()) && getHost_name().equals(kelaiMsg.getHost_name());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFlow_start_time(), getApp(), getServer_total_packet(), getClient_total_byte(), getClient_total_packet(), getNetlink(), getClient_ip_addr(), getTcp_status(), getFlow_end_time(), getClient_port(), getLog_type(), getProtocol(), getServer_port(), getServer_ip_addr(), getServer_total_byte(), getHost_name(), getDirection());
    }

    @Override
    public String toString() {
        return "{" +
                "\"flow_start_time\":" + flow_start_time +
                ", \"app\":" + app +
                ", \"server_total_packet\":" + server_total_packet +
                ", \"client_total_byte\":" + client_total_byte +
                ", \"client_total_packet\":" + client_total_packet +
                ", \"netlink\":" + netlink +
                ", \"client_ip_addr\":\"" + client_ip_addr + "\"" +
                ", \"tcp_status\":" + tcp_status +
                ", \"flow_end_time\":" + flow_end_time +
                ", \"client_port\":" + client_port +
                ", \"log_type\":\"" + log_type + "\"" +
                ", \"protocol\":" + protocol +
                ", \"server_port\":" + server_port +
                ", \"server_ip_addr\":\"" + server_ip_addr + "\"" +
                ", \"server_total_byte\":" + server_total_byte +
                ", \"host_name\":\"" + host_name + "\""+
                ", \"direction\":" + direction + "}";
    }
}
