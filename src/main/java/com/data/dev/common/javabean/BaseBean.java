package com.data.dev.common.javabean;

import java.io.Serializable;

/**
 * @author wangxiaoming-ghq 2022-05-15
 * javabean 及 配置类的基类 主要负责序列化
 */
public abstract class BaseBean implements Serializable {
    public static final long serialVersionUID = 1L;
}
