package com.data.dev.common.javabean.kafkaMailTopic;

import com.data.dev.common.javabean.BaseBean;
import lombok.Data;

import java.util.Objects;

/**
 * {
 *   "user": "yangxuefei-lhb",
 *   "client_ip": "10.68.6.182",
 *   "source": "login",
 *   "loginname": "yangxuefei-lhb@sinosig.com",
 *   "IP": "10.8.148.58",
 *   "timestamp": "17:58:12",
 *   "@timestamp": "2022-04-20T09:58:13.647Z",
 *   "ip": "10.7.231.25",
 *   "clienttype": "POP3",
 *   "result": "success",
 *   "@version": "1"
 * }
 *
 * user登录用户
 * client_ip 来源ip
 * source 类型
 * loginname 登录用户邮箱地址
 * ip 目标前端ip
 * timestamp 发送时间
 * &#064;timestamp  发送日期时间
 * IP 邮件日志发送来源IP
 * clienttype 客户端登录类型
 * result 登录状态
 */
@Data
public class MailMsg extends BaseBean {
    public String user;
    public String client_ip;
    public String source;
    public String loginName;
    public String mailSenderSourceIp;
    public String timestamp_time;
    public String timestamp_datetime;
    public String ip;
    public String clientType;
    public String result;
    public String version;

    public MailMsg() {
    }

    public MailMsg(String user, String client_ip, String source, String loginName, String mailSenderSourceIp, String timestamp_time, String timestamp_datetime, String ip, String clientType, String result, String version) {
        this.user = user;
        this.client_ip = client_ip;
        this.source = source;
        this.loginName = loginName;
        this.mailSenderSourceIp = mailSenderSourceIp;
        this.timestamp_time = timestamp_time;
        this.timestamp_datetime = timestamp_datetime;
        this.ip = ip;
        this.clientType = clientType;
        this.result = result;
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MailMsg)) return false;
        MailMsg mailMsg = (MailMsg) o;
        return getUser().equals(mailMsg.getUser()) && getClient_ip().equals(mailMsg.getClient_ip()) && getSource().equals(mailMsg.getSource()) && getLoginName().equals(mailMsg.getLoginName()) && getMailSenderSourceIp().equals(mailMsg.getMailSenderSourceIp()) && getTimestamp_time().equals(mailMsg.getTimestamp_time()) && getTimestamp_datetime().equals(mailMsg.getTimestamp_datetime()) && getIp().equals(mailMsg.getIp()) && getClientType().equals(mailMsg.getClientType()) && getResult().equals(mailMsg.getResult()) && getVersion().equals(mailMsg.getVersion());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUser(), getClient_ip(), getSource(), getLoginName(), getMailSenderSourceIp(), getTimestamp_time(), getTimestamp_datetime(), getIp(), getClientType(), getResult(), getVersion());
    }

    @Override
    public String toString() {
        return "{" +
                "  'user':'" + user + "'" +
                ", 'client_ip':'" + client_ip  + "'" +
                ", 'source':'" + source  + "'" +
                ", 'loginName':'" + loginName  + "'" +
                ", 'IP':'" + mailSenderSourceIp + "'" +
                ", 'timestamp':'" + timestamp_time + "'" +
                ", '@timestamp':'" + timestamp_datetime + "'" +
                ", 'ip':'"  + "'" +
                ", 'clientType':'" + clientType  + "'" +
                ", 'result':'" + result  + "'" +
                ", 'version':'" + version + "'" +
                "}";
    }

}
