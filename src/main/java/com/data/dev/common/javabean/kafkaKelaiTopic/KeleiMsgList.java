package com.data.dev.common.javabean.kafkaKelaiTopic;

import com.data.dev.common.javabean.BaseBean;
import lombok.Data;

import java.util.List;
import java.util.Objects;

/**
 * log_type 推送类型
 * netlink 链路ID
 * host_name 主机名称
 * flow_start_time 会话开始时间戳
 * flow_end_time 会话结束时间戳
 * client_ip_addr 源IP
 * client_port 源端口
 * server_ip_addr 目标IP
 * server_port 目标端口
 * app 应用ID
 * protocol 协议
 * tcp_status TCP状态
 * client_total_byte client发送字节数
 * server_total_byte server发送字节数
 * client_total_packet client发送数据包数
 * server_total_packet	server发送数据包数
 * direction 流方向
 *
 */
@Data
public class KeleiMsgList extends BaseBean {
    public  List<String> keleiMsgList;

    public KeleiMsgList() {
    }

    public KeleiMsgList(List<String> keleiMsgList) {
        this.keleiMsgList = keleiMsgList;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof KeleiMsgList)) return false;
        KeleiMsgList keleiMsgList = (KeleiMsgList) o;
        return getKeleiMsgList().equals(keleiMsgList.getKeleiMsgList());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getKeleiMsgList());
    }

    @Override
    public String toString() {
        return  keleiMsgList.toString();
    }
}
