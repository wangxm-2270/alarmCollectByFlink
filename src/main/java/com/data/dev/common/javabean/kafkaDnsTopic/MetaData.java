package com.data.dev.common.javabean.kafkaDnsTopic;


import com.data.dev.common.javabean.BaseBean;
import lombok.Data;

import java.util.Objects;

/**
 * 	"@metadata":{
 * 		"beat":"packetbeat",
 * 		"type":"_doc",
 * 		"version":"7.10.2"
 *        },
 */
@Data
public class MetaData  extends BaseBean {

    public String beat;
    public String type;
    public String version;

    public MetaData() {
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MetaData)) return false;
        MetaData metaData = (MetaData) o;
        return getBeat().equals(metaData.getBeat()) && getType().equals(metaData.getType()) && getVersion().equals(metaData.getVersion());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBeat(), getType(), getVersion());
    }

    @Override
    public String toString() {
        return "{" +
                "'beat':'" + beat + "'" +
                ", 'type':'" + type + "'" +
                ", 'version':'" + version+ "'" +
                "}";
    }
}
