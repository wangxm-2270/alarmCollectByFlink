package com.data.dev.common.javabean.kafkaMailTopic;

import com.data.dev.common.javabean.BaseBean;
import lombok.Data;

import java.util.Objects;


/**
 * @author wangxiaoming-ghq 2022-05-15
 * 逻辑统计场景告警事件
 */
@Data
public class MailMsgAlarm extends BaseBean {


    /**
     * 当前登录成功的事件
     */
   public  MailMsg mailMsg;

    /**
     * 当前捕获的告警主键：username@client_ip
     */
   public  String alarmKey;

    /**
     * 第一次登录失败的事件时间
     */
   public  String startTime;

    /**
     * 连续登录失败后下一次登录成功的事件时间
     */
   public  String endTime;

    public  int failTimes;

    @Override
    public String toString() {
        return "{" +
                "  'mailMsg_login_success':'" + mailMsg + "'" +
                ", 'alarmKey':'" + alarmKey + "'" +
                ", 'start_login_time_in3min':'"  +startTime + "'" +
                ", 'end_login_time_in3min':'"  +endTime + "'" +
                ", 'login_fail_times':'"  +failTimes +  "'" +
                "}";
    }

    public MailMsgAlarm() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MailMsgAlarm)) return false;
        MailMsgAlarm that = (MailMsgAlarm) o;
        return getFailTimes() == that.getFailTimes() && getMailMsg().equals(that.getMailMsg()) && getAlarmKey().equals(that.getAlarmKey()) && getStartTime().equals(that.getStartTime()) && getEndTime().equals(that.getEndTime());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMailMsg(), getAlarmKey(), getStartTime(), getEndTime(), getFailTimes());
    }
}

