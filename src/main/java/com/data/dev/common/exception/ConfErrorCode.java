package com.data.dev.common.exception;

/**
 * 2022年6月17日15:10:59
 *@author wangxiaoming-ghq 2022-05-15
 * 该项目的错误码基类
 */
public enum ConfErrorCode implements ErrorCode {

    CONFIG_ERROR("Common-00", "kafka配置错误，请检查kafka配置格式"),
    FLINK_EXECUTE_FAIL("Common-10", "FLINK任务执行失败，请检查FLINK集群及应用配置"),
    HTTP_CHECK_ERROR("Common-13", "查询IP是否生产IP时出错"),
    RUNTIME_ERROR("Common-11", "Hook运行错误"),
    WAIT_TIME_EXCEED("Common-21", "等待时间超出范围"),
    HTTP_PUSH_ERROR("Common-30","推送企业微信时出错" );

    private final String code;

    private final String describe;

    ConfErrorCode(String code, String describe)
    {
        this.code = code;
        this.describe = describe;
    }

    @Override
    public String getCode()
    {
        return this.code;
    }

    @Override
    public String getDescription()
    {
        return this.describe;
    }

    @Override
    public String toString()
    {
        return String.format("Code:[%s], Describe:[%s]", this.code,
                this.describe);
    }
}
