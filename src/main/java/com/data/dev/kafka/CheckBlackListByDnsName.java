package com.data.dev.kafka;


import com.data.dev.key.ConfigurationKey;
import com.data.dev.utils.FileUtils;

import java.util.HashMap;

/**
 * 实现一个静态方法 用来判断是否黑名单，返回boolean
 * @author wangxiaoming-ghq
 * 2022年6月17日15:20:54
 */
public class CheckBlackListByDnsName {

    /**
     *
     * @param dnsName:当前消息中的DNS
     * @return isBlackDns:是否为黑名单DNS
     */
    public static boolean isBlackListDnsName(String  dnsName){
         boolean isBlackListDns;
        HashMap<String, String> blacklistMap;
        blacklistMap = FileUtils.getBlacklistMap(ConfigurationKey.getApplicationProps().get(ConfigurationKey.KAFKA_DNS_QUESTION_BLACKLIST_FILEPATH));
        isBlackListDns = null != blacklistMap.get(dnsName) ;
        return  isBlackListDns;
    }
}
