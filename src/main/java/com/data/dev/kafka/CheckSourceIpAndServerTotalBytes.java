package com.data.dev.kafka;

import com.data.dev.key.ConfigurationKey;
import com.data.dev.utils.FileUtils;
import com.data.dev.utils.IPUtils;

import java.util.HashMap;

/**
 * 筛选：源IP为10.252.0.0/16或者10.249.0.0/16
 */
public class CheckSourceIpAndServerTotalBytes {


    /**
     * 10.252.0.0/16或者10.249.0.0/16
     */
     public static boolean isIpBelongOfCIDR(String ipaddr){
         if(!IPUtils.isIpAddr(ipaddr))
             return false;
       return IPUtils.isInRange(ipaddr, ConfigurationKey.IP_CIDR_252) || IPUtils.isInRange(ipaddr, ConfigurationKey.IP_CIDR_249);
    }

    /**
     * （server_total_byte）大小不超200。
     */

    public static boolean isServerTotalByteLessOf200(int server_total_byte){

        return server_total_byte <= 200;
    }

    /**
     *2022年6月17日15:21:48
     * @param clientIpAddr：当前解析到的IP
     * @return isWhiteListIp:是否为白名单IP
     */
    public static boolean isWhiteListIp(String  clientIpAddr){

        boolean isWhiteListIp;
        HashMap<String, String> whiteListIpMap;
        whiteListIpMap = FileUtils.getWhitelistMap(ConfigurationKey.APPLICATION_PROPERTIES_FILE_PATH );
        isWhiteListIp = whiteListIpMap.get(clientIpAddr) != null;
        return  isWhiteListIp;
    }

}
