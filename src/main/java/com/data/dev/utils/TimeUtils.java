package com.data.dev.utils;

import com.data.dev.common.javabean.BaseBean;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Test;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

/**
 * 2022年6月17日15:35:10
 * @author wangxiaoming-ghq
 * 时间处理全局工具类
 */
public class TimeUtils  extends BaseBean {

    /**
     * 将UTC时间转换为北京时间的timestamp
     * @param UTCDatetimeStr:标准UTC时间
     * @return timestamp:标准北京时间
     */
    public static long switchUTCToBeijingTimestamp(String UTCDatetimeStr) {
        String UTCDatetime = UTCDatetimeStr.replace("Z", " UTC");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS Z");
        Date time = null;
        try {
            time = format.parse(UTCDatetime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assert time != null;

        return time.getTime();

    }

    /**
     *
     * @param UTCDatetimeStr:标准UTC日期时间 yyyy-MM-dd HH:mm:ss
     * @return beijingDatetime:标准北京日期时间 YYYY-MM-DD HH:MM:SS
     */
    public static String switchUTCToBeijingDateTime(String UTCDatetimeStr) {
        String UTCDatetime = UTCDatetimeStr.replace("Z", " UTC");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS Z");
        SimpleDateFormat defaultFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date time;
        String beijingDatetime = null;
        try {
            time = format.parse(UTCDatetime);
            beijingDatetime = defaultFormat.format(time);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return beijingDatetime;
    }

    /**
     *
     * @param time:日期时间
     * @return dateTime:标准北京日期时间 YYYY-MM-DD HH:MM:SS
     */
    public static DateTime switchUTCToBeijingTime(String time) {
        DateTimeFormatter df =  DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

        DateTime dateTime= DateTime.parse(time, df);

        return dateTime;
    }

    @Test
    public void timeTest(){
        String UTC = "2022-04-20T09:35:30.465Z";
        String UTCTime = "2022-04-20T09:35:33.465Z";
        //System.out.println(switchUTCToBeijingTime(switchUTCToBeijingDateTime(UTCTime)));
        System.out.println(switchUTCToBeijingTimestamp(UTCTime)/1000L);
        System.out.println(switchUTCToBeijingTimestamp(UTC)/1000L);

        System.out.println(switchUTCToBeijingTimestamp(UTCTime)/1000L - switchUTCToBeijingTimestamp(UTC)/1000L);
        System.out.println(switchUTCToBeijingDateTime(UTCTime));
        System.out.println(switchUTCToBeijingDateTime(UTC));

        Timestamp.valueOf(String.valueOf(switchUTCToBeijingDateTime(UTCTime)));

        System.out.println( Timestamp.valueOf(String.valueOf(switchUTCToBeijingDateTime(UTCTime))).getTime()/1000L);
    }

    /**
     * @param dateStr:时间字符串
     * @param formatStr：时间
     * @return long:时间戳
     */
    public static Long string2Millis(String dateStr, String formatStr) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formatStr);
            return simpleDateFormat.parse(dateStr).getTime();
        } catch (Exception e) {
            return 0L;
        }
    }


    @Test
    public  void  testtm(){

        DateTime dt = new DateTime(1652241272);
        String str = "2022-04-20 17:55:30";
        DateTimeFormatter df =  DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

        DateTime dateTime= DateTime.parse(str, df);


        //输出验证
        System.out.println(string2Millis(str,"yyyy-MM-dd HH:mm:ss"));

    }

    @Test
    public void testlong(){

        Long timestamp = LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli();
        LocalDateTime time2 =LocalDateTime.ofEpochSecond(1655177452000L/1000L,0,ZoneOffset.ofHours(8));

        System.out.println(LocalDateTime.now());
        System.out.println(timestamp);
        System.out.println(time2);

    }

    public static String switchLongTsToStringDateTime(long ts){
        SimpleDateFormat format =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //设置格式
        String timeText=format.format(ts);

        return timeText;
    }

    public static long switchToEpochMil(LocalDateTime dt) {
        ZoneOffset zoneOffset8 = ZoneOffset.of("+8");
        return dt.toInstant(zoneOffset8).toEpochMilli();
    }

}
