package com.data.dev.utils;

import com.data.dev.common.exception.AlarmException;
import com.data.dev.common.exception.ConfErrorCode;
import com.data.dev.common.javabean.BaseBean;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * 读取配置文件，获取必要信息
 * @author wangxiaoming-ghq
 * 2022年6月17日15:31:23
 */
public class FileUtils extends BaseBean {


    /**
     *
     * @return blacklistMap 返回黑名单的HashMap<String,String>
     */
    public static HashMap<String,String> getBlacklistMap(String blacklistFilePath){

        Path path = Paths.get(blacklistFilePath);
        ArrayList<String> blacklist;
        HashMap<String,String> blacklistMap = new HashMap<>();
        try{
            blacklist = (ArrayList<String>) Files.readAllLines(path, StandardCharsets.UTF_8);
            for(int i=0;i< blacklist.size();i++){
                blacklistMap.put(blacklist.get(i),String.valueOf(i));
            }

        } catch (IOException e) {

            throw AlarmException.asAlarmException(ConfErrorCode.CONFIG_ERROR,
                    e);
        }

        return blacklistMap;
    }


    /**
     *
     * @return blacklistMap 返回白名单IP的HashMap<String,String> : [ip,行号]
     */
    public static HashMap<String,String> getWhitelistMap(String whitelistFilePath){

        Path path = Paths.get(whitelistFilePath);
        ArrayList<String> whitelistIP;
        HashMap<String,String> whitelistMap = new HashMap<>();
        try{
            whitelistIP = (ArrayList<String>) Files.readAllLines(path, StandardCharsets.UTF_8);
            for(int i=0;i< whitelistIP.size();i++){
                whitelistMap.put(whitelistIP.get(i),String.valueOf(i));
            }
        } catch (IOException e) {

            throw AlarmException.asAlarmException(ConfErrorCode.CONFIG_ERROR,
                    e);
        }

        return whitelistMap;
    }

}
